# Elearning Demo Course - Command Line Basics

A demo course crated with Articulate 360.

Link to course: https://yoadrian.gitlab.io/elearning-demo-cline 

## Information

The following course was created by Adrian Dobre for demonstration purposes only. 

The structure of the course was inspired by the first chapter of Mark Bates' Conquering the Command Line. Text in between quotations marks is from the aforementioned book, if not otherwise stated. The video in the first lesson is licensed as Creative Commons Attribution license (reuse allowed) by Barton Poulson.



